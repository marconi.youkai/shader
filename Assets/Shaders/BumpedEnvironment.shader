﻿Shader "Holistic/BumpedEnvironment"
{
    Properties {
        _baseColor ("Base color map", 2D) = "white" {}
        _normal ("Normal map", 2D) = "bump" {}
        _normalStrength ("Normal map strength", Range(0,10)) = 1
        _brightness ("Brightness", Range(0,10)) = 1
        _cubeMap ("Cube map", CUBE) = "white" {}
    }
    
    SubShader {
    
        CGPROGRAM
            #pragma surface surf Lambert
            
            sampler2D _baseColor;
            sampler2D _normal;
            half _normalStrength;
            half _brightness;
            samplerCUBE _cubeMap;
            
            struct Input {
                float2 uv_baseColor;
                float2 uv_normal;
                float3 worldRefl; INTERNAL_DATA
            }; 
            
            void surf ( Input IN, inout SurfaceOutput o) {
                o.Albedo = tex2D ( _baseColor, IN.uv_baseColor).rgb;
                o.Normal = UnpackNormal(tex2D( _normal, IN.uv_normal)) * _brightness;
                o.Normal *= float3( _normalStrength, _normalStrength, 1);
                o.Emission = texCUBE(_cubeMap, WorldReflectionVector(IN, o.Normal)).rgb; 
            } 
        ENDCG
    }
    Fallback "Diffuse"
}