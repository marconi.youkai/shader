﻿Shader "Holistic/BumpDiffuse"
{
   Properties{
        _color ("Color Map", 2D) = "white" {}
        _normal ("Normal Map", 2D) = "bump" {}
        _normalStrength ("Normal strength", Range (0,10)) = 1 
        _brightness ("Brightness strength" , Range (0,10)) = 1
   }
    
    Subshader {
        CGPROGRAM
            #pragma surface surf Lambert
            
            sampler2D _color;
            sampler2D _normal;
            half _normalStrength;
            half _brightness;
            
            struct Input {
                float2 uv_color;
                float2 uv_normal;
            };
            
            void surf ( Input IN, inout SurfaceOutput o){
                o.Albedo = tex2D ( _color, IN.uv_color).rgb;
                o.Normal = UnpackNormal( tex2D ( _normal, IN.uv_normal));
                o.Normal *= float3(_normalStrength, _normalStrength, 1) * _brightness;
            }
        ENDCG
    }   
    Fallback "Diffuse"
}
