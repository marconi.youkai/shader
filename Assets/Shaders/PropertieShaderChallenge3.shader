﻿Shader "Holistic/PropertiesChallenge3" {
    Properties{
        _myTex ("Texture" , 2D) = "White" {}
    }
    SubShader {
        CGPROGRAM
            #pragma surface surf Lambert
            
           sampler2D _myTex;
            
            struct Input {
                float2 uv_myTex;
            };
            
            void surf ( Input IN, inout SurfaceOutput o){
                float4 sampledTexture = tex2D(_myTex, IN.uv_myTex);
                float4 green = float4 (0,1,0,1);
                sampledTexture *= green;
                o.Albedo = sampledTexture.rgb;
            }
            
        ENDCG
    }
    FallBack "Diffuse"
}