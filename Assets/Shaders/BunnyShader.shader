﻿Shader "Holistic/HelloShader"{
    Properties {
        _myColor ("Exemple Color", Color) = (1,1,1,1)
        _emissionColor ("Exemple Emission Color", Color) = (1,1,1,1)
        _myNormal ("Normal Color", Color) = (1,1,1,1)
    }
    
    SubShader {
        CGPROGRAM
            #pragma surface surf Lambert
            
            struct Input{
                float2 uvMainTex;
            };
            
            fixed4 _myColor;
            fixed4 _emissionColor;
            fixed4 _myNormal;
            
            void surf (Input IN, inout SurfaceOutput o) {
                
                o.Albedo = _myColor.rgb;
                o.Emission = _emissionColor.rgb;
                o.Normal = _myNormal.rgb;
            }
            
            ENDCG
    }
    
    FallBack "Duffuse"
}