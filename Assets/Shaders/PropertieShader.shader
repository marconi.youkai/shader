﻿Shader "Holistic/AllProps" {
    Properties{
        _myColor ("Color", Color) = (1,1,1,1)
        _myRange ("Color multiplier", Range(0,5)) = 1 
        _myTex ("Texture" , 2D) = "White" {}
        _myCube ("CubeMap", CUBE) = "" {}
        _myFloat ("Float", Float) = 0.5
        _myVector ("Vector", Vector) = (0.5,1,1,1)
    }
    SubShader {
        CGPROGRAM
            #pragma surface surf Lambert
            
            fixed4 _myColor;
            half _myRange;
            sampler2D _myTex;
            samplerCUBE _myCube;
            float _myFloat;
            float4 _myVector;
            
            struct Input {
                float2 uv_myTex;
                float3 worldRefl;
            };
            
            void surf ( Input IN, inout SurfaceOutput o){
                //o.Albedo = (tex2D(_myTex, IN.uv_myTex) + _myColor).rgb;
                float4 sampledTexture = tex2D(_myTex, IN.uv_myTex);
                sampledTexture.g *= 1;
                o.Albedo = sampledTexture.rgb;
            }
            
        ENDCG
    }
    FallBack "Diffuse"
}