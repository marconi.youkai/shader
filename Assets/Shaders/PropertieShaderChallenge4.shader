﻿Shader "Holistic/PropertieShaderChallenge4"
{
    Properties{
        _diffuse ("Diffuse texture",  2D) = "White" {}
        _emissive ("Emissive texture", 2D) = "White" {}
    }
    
    SubShader{
        CGPROGRAM
            #pragma surface surf Lambert
            
            sampler2D _diffuse;
            sampler2D _emissive;
            
            struct Input{
                float2 uv_diffuse;
                float2 uv_emissive;
            };
            
            void surf (Input IN, inout SurfaceOutput o){
                float4 diffuseColor = tex2D ( _diffuse, IN.uv_diffuse);
                float4 emissiveColor = tex2D ( _emissive, IN.uv_emissive);
                o.Albedo = diffuseColor.rgb;
                o.Emission = (emissiveColor).rgb;
            } 
            
        ENDCG
    
    }

   Fallback "Diffuse"
}
