﻿Shader "Holistic/Rim"{
    Properties{
        _baseTexture ( "Base texture", 2d) = "white" {}
        _RimColor ( "Rim color", Color) = (0.0 , 0.5 , 0.5, 0.0) 
        _RimPower ( "Rim power", Range ( 0.5 , 8.0 ) )  = 1.0 
        _stepColor1 ( "Step Color 1", Color) = (1.0 , 0.0 , 0.0, 1.0)
        _stepColor2 ( "Step Color 2", Color) = (0.0 , 2.0 , 0.0, 1.0)
        _colorValueDim ( "color Value Dim", Range ( 0.0 , 1.0 ) ) = 0.5
        _stepSize ( "Step size", Range ( -8.0 , 8.0 ) )  = 1.0 
    }
    
    SubShader{
        CGPROGRAM
        #pragma surface surf Lambert

        struct Input{
            float2 uv_baseTexture;
            float3 viewDir;
            float3 worldPos;
        };

        sampler2D _baseTexture;
        float4 _RimColor;
        half _RimPower;
        float4 _stepColor1;
        float4 _stepColor2;
        half _colorValueDim;
        half _stepSize;
        
        void surf( Input IN, inout SurfaceOutput o) {
            float4 tex = tex2D( _baseTexture, IN.uv_baseTexture);
            half rim = 1 - saturate( dot(normalize(IN.viewDir), o.Normal));
            half xStepResult = step ( frac( IN.worldPos.x * 20  * 0.5 ) - _stepSize , _stepSize);
            half yStepResult = step ( frac( IN.worldPos.y * 20  * 0.5 ) - _stepSize , _stepSize);
            half zStepResult = step ( frac( IN.worldPos.z * 20  * 0.5 ) - _stepSize , _stepSize);
            half absStepResult = xStepResult + yStepResult + zStepResult;
            
            o.Albedo = tex;
            s
            //o.Emission = (_stepColor1 * yStepResult) + (_stepColor1 * _colorValueDim * (1 - yStepResult)) + (_stepColor2 * xStepResult) + (_stepColor2 * _colorValueDim * ( 1 - xStepResult ) );
            o.Emission = (_stepColor1 * absStepResult) + (_stepColor1 * _colorValueDim * (1 - absStepResult)) + (_stepColor2 * absStepResult) + (_stepColor2 * _colorValueDim * ( 1 - absStepResult ) );
            
            //Rim lightning
            //o.Emission =  _RimColor.rgb * (1 - step( rim ,  _RimPower ) ) ;
        }
        
        ENDCG
    }
    Fallback "Diffuse"
}